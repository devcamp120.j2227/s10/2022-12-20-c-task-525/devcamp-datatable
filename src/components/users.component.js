import { Container, Grid, TableContainer, Paper, Table, TableHead, TableRow, TableBody, TableCell, CircularProgress, Pagination } from "@mui/material";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { fetchUsers, changePagination } from "../actions/users.actions";

const Users = () => {
    const dispatch = useDispatch();

    const limit = 3;

    const { users, pending, currentPage, totalUser } = useSelector((reduxData) => reduxData.userReducers);

    const noPage = Math.ceil(totalUser / limit);

    useEffect(() => {
        dispatch(fetchUsers(limit, currentPage));
    }, [currentPage]);

    const onChangePagination = (event, value) => {
        dispatch(changePagination(value)); 
    }

    return (
        <Container>
            <Grid container mt={5}>
                { pending ? 
                    <Grid item md={12} sm={12} lg={12} xs={12} textAlign="center">
                        <CircularProgress />
                    </Grid>
                    :
                    <React.Fragment>
                        <Grid item md={12} sm={12} lg={12} xs={12}>
                            <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Id</TableCell>
                                            <TableCell>Name</TableCell>
                                            <TableCell>Username</TableCell>
                                            <TableCell>Email</TableCell>
                                            <TableCell>Phone</TableCell>
                                            <TableCell>Website</TableCell>
                                        </TableRow>
                                    </TableHead>        
                                    <TableBody>
                                        {users.map((element, index) => {
                                            return <>
                                                <TableRow key={index}>
                                                    <TableCell>{element.id}</TableCell>
                                                    <TableCell>{element.name}</TableCell>
                                                    <TableCell>{element.username}</TableCell>
                                                    <TableCell>{element.email}</TableCell>
                                                    <TableCell>{element.phone}</TableCell>
                                                    <TableCell>{element.website}</TableCell>
                                                </TableRow>
                                            </>
                                        })}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                        <Grid item md={12} sm={12} lg={12} xs={12} mt={5}>
                            <Pagination count={noPage} defaultPage={currentPage} onChange={onChangePagination}/>
                        </Grid>
                    </React.Fragment>
                }
            </Grid>
        </Container>
    )
}

export default Users;